#!/bin/bash

echo ""
read -p "Enter path to Arduino SDK: "
if [ -d "$REPLY" ]; then
    SDK=$REPLY
    if [ -d "$SDK/libraries" ] ; then
        cp -r libs/* $SDK/libraries
        if [ -d "$SDK/libraries/watchduino_core" ] ; then
            rm -r $SDK/libraries/watchduino_core/
        fi
        mkdir $SDK/libraries/watchduino_core
        cp -r src/core/* $SDK/libraries/watchduino_core
    else
        echo "Path is not a valid Arduino SDK"
        exit 1
    fi
    
else 
    echo "Path does not exist! (or it's not a directory)"
    exit 1
fi