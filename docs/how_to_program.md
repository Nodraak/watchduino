# How to program WatchDuino

A smartwatch wouldn't be such if you couldn't program it. There is two ways you can program WatchDuino:

- Providing your own customizations (custom apps, custom alarm).
- Hacking on the system itself.

In both cases, the first step you should do is getting the WatchDuino source code, and getting it ready to build. Refer to the [install document](install.md) for details on how to do this.
    
It's also a good idea if you build a physical WatchDuino first, even if it is on a breadboard, so that you can try the code that you make. To do so, refer to the [how to replicate document](how_to_replicate.md).

## Providing customizations

You can use WatchDuino as a framework, and program your own customizations to plug on it.

On the WatchDuino source code, you will find a [file under the `watch` directory](../src/watch/watch.ino). This is the main file in WatchDuino, but try to open it in a text editor and you'll see that it's barely anything more than a template. All of WatchDuino's functionality is really present in [the core library](../src/core). The `sysloop` function that you are calling in the main method is really the one that does everything. But notice that you can pass it certain resources as arguments: this is how you can provide customizations. You only have to write those resources in the format WatchDuino likes, and then hand them over to the `sysloop`, and they will be plugged into the system.

We will now guide you into the process of building your customizations. If you are one of those people who are bored by docs and feel brave enough to directly jump into the action, you can have a look at the [examples directory](../src/examples), where you'll find the result of the examples we are going to build here.

To begin with, let's create a new independent project (your customized WatchDuino), and make it compile.

## Create your project

You have two options: the regular one is to use the regular Arduino tooling, and the alternative is to manage everything using [Biicode](http://biicode.com).

### Create a custom Arduino sketch

Create a directory of the name you choose, then make a file with the same name and a `.ino` extension. For instance: `example/example.ino`. Then copy the contents of the [`watch.ino`](../src/watch/watch.ino) file, and paste them into the one you just made.

Fire up the Arduino IDE, and open your `example.ino` file. Click on "verify", and if you have your installation right, it should compile without errors (if not, go and check the [install document](install.md) again, because you probably need to fix something in your setup). If you upload your sketch into your physical WatchDuino, you should see the same as with the vanilla one (because you haven't done any customizations yet, obviously).

You are now ready to start hacking.

### Create a custom Biicode project

If you are a Biicode user, start by creating a new Arduino hive:

    $ bii new
    Introduce name: WatchDuino_example
    Created new hive WatchDuino_example
    Select language: (java/node/fortran/python/cpp/arduino/None)
    Introduce lang (default:None): arduino
    INFO: Selected lang: arduino
    How would you like to name your first block?
    Introduce block name: example
    INFO: block name: example
    Generate a default firmware?  (YES/no) no

Make sure you configure your workspace with your SDK path and your serial port. Follow [instructions on Biicode docs](http://docs.biicode.com/arduino/arduino-set-up.html).
    
Then make a new file called `example.cpp` (or whatever you like really) into your block. Copy the contents of the [`main.cpp`](../biicode_hive/blocks/coconauts/watchduino/main.cpp) file that you will find the `biicode_hive/blocks/coconauts/watchduino` directory, and paste them into the `example.cpp` file you just made.

Now make Biicode grab the dependencies:

    bii find
    
And you should now be able to compile the code:

    bii Arduino:build
    
Try uploading it to your physical WatchDuino as well:

    bii Arduino:upload

If you see the watch interface, then great, you are ready to start changing it.

### Changing the alarm melody

This is probably the easiest customization you can make. WatchDuino lets you set alarms and timers, playing a melody when the time is due. You can replace the default melody with a custom one with a custom one.

In the `watch.ino` file, you will find a block of code containing two arrays and an integer variable:

    const int alarm_melody[] = {  NOTE_B4, NOTE_B4, NOTE_B4, NOTE_B4, NOTE_G4, NOTE_A4, NOTE_B4 , NOTE_A4, NOTE_B4};
    const int alarm_durations[] = { 1, 1, 1, 4 , 4, 4, 2, 1, 4 };
    const int ALARM_SIZE = 9;

That's the data structures that make up the alarm. You will notice it's broken out in three: one holding the melody (i.e., the tones) and another holding the durations of the tones. 

The tones are constants defined in the [`core/notes.h`](../src/core/notes.h) file (`watch.ino` includes this file in fact). If you open this file, you can see the available values you can use. You can see that the constants are actually mapped to integer values, that actually represent audible frequencies, but it's easier to refer to them by name when you're building melodies. The names are in the pattern `NOTE_<letter><number>`. The letter represents a pitch class (A, B, C, D, E, F, or G), and the number represents the octave number. A particular pitch class in a particular octave is a unique identifier for a tone.

The durations are plain integers, and each of them should correspond to the tone in the same position in the melody array (both arrays should therefore be of the same length). A value of 1 represents a duration of one beat, and any other number will be a multiple. So a duration of 2 will make the corresponding tone sound for two beats, and a duration of 5 will make it sound for five beats. No rocket science here.

Finally, the `ALARM_SIZE` constant should tell us the length of the melody arrays (which, as we said, should be the same for both). It may seem redundant information, but the system needs it for efficiency purposes. In the default melody we have nine notes in the arrays, and thus the `ALARM_SIZE` is 9.

Let's change the melody with a different one. I've picked a very simple public domain song:

    const int alarm_melody[] = {  NOTE_G4, NOTE_G4, NOTE_G4, NOTE_D4, NOTE_E4, NOTE_E4, NOTE_D4 , NOTE_B4, NOTE_B4, NOTE_A4, NOTE_A4, NOTE_G4};
    const int alarm_durations[] = { 1, 1, 1, 1, 1, 1, 3, 1, 1, 1, 1, 3};
    const int ALARM_SIZE = 12;
    
Right, so if you take a look at the main function you will see how the three data structures are fed into the `sysloop` as parameters. If you now compile the code, upload it to your WatchDuino, and then set a new alarm using the interface, and wait until it is due, the new melody will pop out!
    
### Making a custom "app"

What really defines "smart stuff" is basically that you can program apps for them. So same goes for WatchDuino. In WatchDuino's source code lingo, apps are called *screens*. This is tightly coupled to the way the WatchDuino interface works: by looping over different classes that take control of providing the screen contents.

The system core already includes some screens: the main watch, settings, and a couple of games. But in the `watch.ino` file, you can see that you have an empty array called `custom_screens`, that you feed into the sysloop. If you plug your own screens into this array, they will show up when you traverse screens in the watch interface, right after the core ones.

To make a screen you have to extend the `Screen` abstract class, which is defined in [`core/screen.h`](../src/core/screen.h), and implement the required methods, and then you will be able to instantiate it and put it into the `custom_screens` array. Let's see an example:

    class Test: public Screen 
    {
        public:
        void draw(){
            // I get called in every frame, and should paint things into the display
        }
        void controller() {
            // I should respond to input from the buttons
        }
        void update(){
            // I get called in every frame, and should update the state if needed
        }
        void enter() {
            // I get called when the user enters the screen
        }
        void exit() {
            // I get called when the user leaves the screen
        }
    };

    Screen *custom_screens[] ={ 
        new Test()
    };

Nice, so we have created a dummy screen that does nothing (because all of the methods are empty!). If you compile and upload your code into WatchDuino, if you loop over the screens, you will eventually reach yours, but it shows nothing, how boring! Let's try to fix that.

You will notice some comments below each method in the example, which briefly explain what they do. You may also notice that many of them refer to things done "every frame", what does this mean? Well, WatchDuino's system is basically an infinite loop (the `sysloop` name might make some sense now). During that loop, what happens is roughly this:

- draw things into the screen (depending on state)
- check input from the controllers
- update the state

This actions are delegated to whatever screen is in control at the moment, which is why the screen interface includes methods for dealing with this actions. Each iteration of the loop is called *a frame*, so that is why we say that those three methods are called *on every frame*.

WatchDuino runs at ten frames per second by default (so every second, the loop will repeat ten times). This is dictated by the protected `refresh` instance variable in the `Screen` class, that by default gets initialized to 100. This represents the number of milliseconds that will lapse between screen refreshes, so in practice this means that during a second (1000 milliseconds) the screen will refresh 10 times. Yo can override that value in your custom screens to make it suit your needs.

#### Hello world

Now let's actually do something interesting. For instance, let's write "Hello world" into the screen. To define what will be painted into the screen, we will use the `draw` method:

    void draw(){
        display.setCursor(8,HEIGHT/2);
        display.println("Hello world!");
    }

Hold on, what magic is this? Where does that `display` come from? Actually, from the WatchDuino core interface, defined in the file [`watchcore.h`](../src/core/watchcore.h):

    extern Adafruit_PCD8544 display;
    
So we see that display is a global variable, an object of class `Adafruit_PCD8544`, which is Adafruit's official class for dealing with the model of Nokia screens that WatchDuino uses. You can use the API provided by that class to deal with the display. You can find the documentation at [the project's website](https://github.com/adafruit/Adafruit-PCD8544-Nokia-5110-LCD-library). In our case, we are simply placing the cursor in a position around the middle of the screen, and then writing the text "Hello world!" into it. 

If you go and have a view at [`watchcore.h`](../src/core/watchcore.h), you'll find some other global variables and helper functions that you can use when writing your own screens, just like you did with the `display`variable. Unfortunately, due to WatchDuino being in an early state of development, with a quickly changing API, we don't have any in-depth documentation about those variables and methods at the moment. So the best approach is to have a look at the WatchDuino core implementation in [`watchcore.cpp`](../src/core/watchcore.cpp) to figure out what they do, and see some examples of the core screens using them.

#### An alarm tester

We've seen that we can use the full API of `Adafruit_PCD8544` to deal with the screen. But our apps can also access a bunch of other WatchDuino core resources. In fact, all of those declared in the [`watchcore.h`](../src/core/watchcore.h) interface, which include a bunch of functions and attributes.

Let's make a small example with them. Remember when we made the custom alarm? It was really a PITA to test it, having to manually program it through the interface. We could make a screen that would set off the alarm when we enter, so that we can hear it without having to do anything. 

Let's reopen the main file for the alarm example we made earlier, to add a custom screen into it:

    class AlarmTest: public Screen 
    {
        public:
        void draw(WatchDisplay display){
            display.setCursor(10,HEIGHT/2);
            display.println("Alarm test");
        }
        void controller() {}
        void update(){}
        void enter() {
            alarm_active = true;
            alarm = now();
        }
        void exit() {
            alarm_active = false;
        }
    };

    Screen *custom_screens[] ={ 
        new AlarmTest()
    };

The `draw` method is almost identical to the one in our hello world example, and we just put it there for informative purposes. The real deal happens on the `enter` and `exit` methods, where we are accessing global core variables declared on `watchcore.h`. In WatchDuino, what controls he triggering of the alarm are two conditions: the alarm has to be activated, and the alarm timer must have reached the current moment in time. This is precisely what we are simulating in the `enter` method. Then, on the `exit` method we are setting the alarm back to inactive so that everything goes back to normal.

## Hacking on the software

Since WatchDuino's software is open source, you have the full source code at your disposal to make any modifications you like.

If you have read through the previous section of the tutorial detailing how to make customizations, you should have a general idea on how the software is structured: a main loop, independent classes extending `Screen` providing the functionality, and usage of the `Adafruit_PCD8544` class as our base for dealing with the screen. Core functionality is abstracted as a library, that gets called by a main program providing customizations.

There's other details we've left out, but probably the best way to figure out things at this point is directly diving into the code. At this level you have more flexibility and control than the one offered by the framework.

If you make any modifications that you think would enhance the framework and would like to contribute them, don't be afraid of making a pull request!