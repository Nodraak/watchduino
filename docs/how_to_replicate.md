# How to replicate WatchDuino

## Get the components

WatchDuino is made out of the following hardware components:

- ATMega 328 with Arduino boot - [8$ at DealExtreme](http://dx.com/p/aaron-1-atmega328-pu-atmel-microcontroller-chip-for-uno-r3-uno-328-2pcs-267118?Utm_rid=20371386&Utm_source=affiliate)
- Crystal oscillator (16Mhz) [3.66$ at DealExtreme](http://dx.com/p/electronic-diy-16mhz-crystal-oscillators-silver-20-piece-pack-147074?Utm_rid=20371386&Utm_source=affiliate)
- LiPo battery (240mAh) - [2.40$ at DealExtreme](http://dx.com/p/502030-240mah-rechargeable-polymer-li-ion-battery-silver-227107?Utm_rid=20371386&Utm_source=affiliate) 
- LiPo battery charger (usb) - [1.87$ at DealExtreme](http://dx.com/p/1a-lithium-battery-charging-module-blue-205188?Utm_rid=20371386&Utm_source=affiliate)
- Nokia 5110 LCD screen - [3.98$ at DealExtreme](http://dx.com/p/replacement-1-6-lcd-screen-with-blue-backlight-for-nokia-5110-blue-145860?Utm_rid=20371386&Utm_source=affiliate)
- 6 x Mini push buttons [3$ at DealExtreme](http://dx.com/p/2-pin-electrical-power-control-push-button-switch-20-piece-pack-143004?Utm_rid=20371386&Utm_source=affiliate)
- Breadboard [4.22$ at DealExtreme](http://dx.com/p/300-tie-points-prototype-solderless-breadboard-white-118829?Utm_rid=20371386&Utm_source=affiliate)
- Stripboard [3.26$ at DealExtreme](http://dx.com/p/prototype-universal-printed-circuit-board-breadboard-brown-5-piece-pack-130926?Utm_rid=20371386&Utm_source=affiliate)
- Jump Wires [from 2.50$ at DealExtreme](http://dx.com/s/jump%2bwires?PriceSort=up&Utm_rid=20371386&Utm_source=affiliate)
- Buzzer [from 2$ at DealExtreme](http://dx.com/s/buzzer?PriceSort=up&Utm_rid=20371386&Utm_source=affiliate)
- Any 4 pin connector (LiPo /Usb mini) [2$ at DealExtreme](http://dx.com/p/bonatech-03120229-xh2-54-4-pin-w-connectors-plug-straight-needle-seat-terminals-10pcs-302604?Utm_rid=20371386&Utm_source=affiliate)

We've included links to the products in DealExtreme so can make your order all from the same place. DealExtreme ships for free to the whole world, and is quite reliable, though it takes some time to arrive (sometimes even around one month). You can pay an extra for express delivery if you're impatient though!

There's many other electronics shops that may be more convenient for you depending on where you live. Some popular ones include [Sparkfun](http://www.sparkfun.com) in the US, or [Cooking Hacks](http://www.cooking-hacks.com) in Europe.

The prices might fluctuate depending on the specific components you buy, but overall, the total cost for your shopping should be around 30$, and that including a lot of spare components (cables, buttons...) that come in a pack and you will not need.

## Get the code and software

Refer to the [install document](install.md).

## Mount on a breadboard

The code alone will be no good if you don't have some hardware to put it into. If you want to quickly put together the hardware for development, the best way is to mount everything on a breadboard. You won't be able to wear the watch on you hand, but no soldering is required, and you can easily undo any mistakes. Even if you're planning to build the real thing, by mounting a prototype on a breadboard will make sure that everything works as it should.

As we have said before, WatchDuino doesn't really rely on the Arduino board: it only requires it's "brain", the ATMega 328 chip. If you have an Arduino Uno, it will come with an ATMega plugged on it. You will have to remove it by gently pulling it upwards, because you'll need to make your connections directly to the ATMega's pins, rather than to the Arduino ones. 

The lobotomized Arduino board will still be used for development. We will make use of the Arduino serial connection to transfer the code from your PC. For this magic to work, you'll use the RX and TX pins on the Arduino board, and connect them to the respective pins on the ATMega (D0 and D1). The reset pin on the Arduino also needs to be connected to the reset pin on the ATMega, and of course, the ground and VCC pins need to be connected between them as well. 

The idea is exemplified by this image (the rest of the components on the breadboard aside from the ATMega are not really meaningful for our purposes, but it shows the link between the Arduino board and the chip quite clearly):

![Serial-connecting an Arduino board and an ATMega chip](http://coconauts.net/web/img/watchduino/ArduinoUSBSerial.png)

Here's a couple of nice tutorials giving some more detail on how this process works, if you're feeling curious: 
- http://Arduino.cc/en/Tutorial/ArduinoToBreadboard
- http://www.openhomeautomation.net/arduino-breadboard/

In the WatchDuino repository, under the [`schema`](../schema/fzz schema) folder, you will find the schematics describing the circuitry of the device. These are provided as both Fritzing format, and png images. Three views are provided: breadboard, schema, and pcb. They all describe the same information, but as you're mounting a breadboard prototype, the breadboard view is the one that you should use as a reference. Simply follow the connections described, and you should end up with a working prototype of the circuit.

Although you could do everything just looking at the png images, we recommend you to install Fritzing to view the .fzz schematics. It's free and open source, and available for all mayor operating systems. You'll get them in higher quality, interactive, and will be able to identify the pins more easily. You can download the client from [http://fritzing.org/](their webpage), and then use it to open the .fzz files.

## Upload the software to your hardware

You are now ready to upload the software to the prototype you just built. You'll need to have the Arduino SDK and IDE installed in your computer. We already pointed you at [the Arduino software site](http://Arduino.cc/es/main/software) with downloads and instructions on a previous section. Don't forget to also copy the dependency libraries (the ones in the [`libs`](../libs) directory in your repository clone) to your `libraries` folder in your SDK installation.

Connect the Arduino board attached to your prototype to your pc using a USB cable. Open the Arduino IDE, make sure that you have your serial port set up, and open the [`watch.ino`](../src/watch/watch.ino) file (you'll find it under the `src/watch` folder on your repository clone). The IDE should then automatically open the rest of the files that make up the project. Now you can use the verify button to compile, and the upload button to transfer the program to your WatchDuino. 

If you see any errors at this point, it might be that you forgot to copy the dependencies into the Arduino SDK libraries folder (we told you two times already!), or it might be something wrong with your setup. Try compiling and loading into your Arduino one of the examples that come with the SDK, and see if you have everything right.

If all works well, you should hear a bleep and see the WatchDuino interface come up on your Nokia screen, awesome! Try that all of buttons work as well. If you don't see anything on the screen, you might have connected your prototype wrong, and you'll have to double check it against the schematic.

## Build the real thing

Once you have your prototype working, you might want to go on and build a REAL WatchDuino. You'll basically have to replicate the same connections as you did when building your prototype, only this time you'll have do some soldering and plan carefully your design. We made ours by using a stripboard as a base, placing the Nokia screen on one face and soldering all of the connections from the other, and we placed the buttons on the sides. It ended up looking something like this:

![Our WatchDuino's innards](http://coconauts.net/web/img/watchduino/watchduino-soldered.jpg =500px)

In your RealLife model you can't have your Arduino board permanently plugged to the WatchDuino. In fact, you'll only need to connect to it if you want to reprogram it. When you build your actual WatchDuino you can leave them exposed somehow so that you can reprogram the WatchDuino. What we did with ours is make use of a LiPo battery connector (like the ones we pointed you at [in the shopping section](http://dx.com/p/bonatech-03120229-xh2-54-4-pin-w-connectors-plug-straight-needle-seat-terminals-10pcs-302604)), which leaves some pins exposed to the exterior of the watch so that you can plug jump wires back to the Arduino. We also attached a usb charging module to the battery, which we exposed to the exterior as well, to be able to recharge the it.

You may also want to build a case and a strap for it. We made our own out of velcro strips, which makes for an easy, inexpensive, yet comfortable and aesthetic strap.

If you've built your own WatchDuino and made a very cool design you would like to share with the world, let us know!