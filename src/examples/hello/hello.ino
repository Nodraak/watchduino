#include <watchcore.h>
#include <screen.h>
#include <notes.h>

#include <Adafruit_GFX.h> 
#include <Adafruit_PCD8544.h> 
#include <Time.h> 
#include <JeeLib.h> 

class Test: public Screen 
{
    public:
    void draw(){
        display.setCursor(8,HEIGHT/2);
        display.println("Hello world!");
    }
    void controller() {}
    void update(){}
    void enter() {}
    void exit() {}
};

Screen *custom_screens[] ={ 
    new Test()
};

// default melody, ff fanfarre
const int alarm_melody[] = {  NOTE_B4, NOTE_B4, NOTE_B4, NOTE_B4, NOTE_G4, NOTE_A4, NOTE_B4 , NOTE_A4, NOTE_B4};
const int alarm_durations[] = { 1, 1, 1, 4 , 4, 4, 2, 1, 4 };
const int ALARM_SIZE = 9;

int main(void) {
    return sysloop(custom_screens, alarm_melody, alarm_durations, ALARM_SIZE);
}