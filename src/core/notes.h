 
#define notes_h

#define NOTE_C4  262 //do
#define NOTE_CS4 277
#define NOTE_D4  294 //re
#define NOTE_DS4 311
#define NOTE_E4  330 //mi 
#define NOTE_F4  349 //fa 
#define NOTE_FS4 370
#define NOTE_G4  392 //sol 
#define NOTE_GS4 415
#define NOTE_A4  440 //la 
#define NOTE_AS4 466
#define NOTE_B4  494 //si
