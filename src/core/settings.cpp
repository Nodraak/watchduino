#include "settings.h"
#include "utils.h"
#include "watchcore.h"


#include <Adafruit_GFX.h>
#include <Adafruit_PCD8544.h>


void Settings::draw(){
    display.drawRoundRect(0,10+10*selectedSettings,35,11,2 , BLACK);

    display.setCursor(2,12); display.println("Sleep");
    display.setCursor(40,12); display.println(sleep?YES_MSG:NO_MSG);

    display.setCursor(2,22);  display.println("Sound");
    display.setCursor(40,22); display.println(sound?YES_MSG:NO_MSG);

    display.setCursor(2,32);  display.println("Gap");
    display.setCursor(40,32); display.println(String(fixTime)+" s");
}

void Settings::controller(){
    if (pushedButton(SELECT_PIN)) selectedSettings = (selectedSettings+1) %3;
    if (pushedButton(UP_PIN)) switch(selectedSettings){
        case 0: sleep = !sleep; break;
        case 1: sound = !sound ;break;
    } 
    if (pushedButton(DOWN_PIN)) switch(selectedSettings){
        case 0: sleep = !sleep; break;
        case 1: sound = !sound ; break;
    }   
    
    if (pressedButton(UP_PIN)) switch(selectedSettings){
        case 2: fixTime++; break;
    } 
    if (pressedButton(DOWN_PIN)) switch(selectedSettings){
        case 2: fixTime--; break;
    }   
}
void Settings::update(){ }
void Settings::enter() {}
void Settings::exit() {}
