#include "utils.h"

bool  wasPushed[13];

String getDigit(int digits)
{
  if(digits < 10)
    return "0"+String(digits);
  else
    return String(digits);
}


bool pressedButton(int pin){
    return digitalRead(pin) == LOW;
}

bool pushedButton(int pin){
    if (digitalRead(pin) == LOW){
        if (!wasPushed[pin]){
            wasPushed[pin] = true;
            return true;
        }
        return false;
    } else {
        wasPushed[pin] = false;
        return false;
    }
}

int clock12ToDeg(int clockVal) {
    if (clockVal >= 12) clockVal = clockVal-12; 
    return  90 - (30 * clockVal);
}
int clock60ToDeg(int clockVal){
    if (clockVal >= 60) clockVal = clockVal-60; 
    return  90 - (6 * clockVal);
}

int degToClockX(int degree, int r){
    return  r * cos(rad(degree));
}
int degToClockY(int degree, int r){
    return  r * sin(rad(degree));
}

float rad (int degree){
    return degree* (3.1416/180);
 }