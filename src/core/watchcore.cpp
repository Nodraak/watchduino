/*
*Smart watch developed by Coconauts
*/

#include <WString.h>
#include <Adafruit_GFX.h> 
#include <Adafruit_PCD8544.h> 
#include <Time.h>   
#include <JeeLib.h> 
#include "notes.h"
#include "images.h"
#include "utils.h"
#include "screen.h"
#include "watchcore.h"
#include "settings.h"

//SLEEP VARIABLES
ISR(WDT_vect) { Sleepy::watchdogEvent(); }
const int WAKE_PIN = 2;                 // pin used for waking up
int count = 0;                   // counter
int refresh = 1000;
const int SLEEP_TIME = 1000/refresh * 30;

//TESTING
const int LED = 13;
bool DEBUG = false;
String debug_str = "";

//SOUND VARIABLES
const int BUZZERPIN = 11;
const int NOTE_DURATION = 1000 / 4; //a quarter
const int DELAY_BETWEEN_NOTES =1000; //ms

//SCREEN VARIABLES
Adafruit_PCD8544 display = Adafruit_PCD8544(8,7,6,4,5);
int contrast = 60;

int screen = 0; 

//ALARM 
bool alarm_active = 0; // 0 = inactive  
int alarm_mode = 0; // 0 = hour_alarm (ej: alarm at 5:52)
                   // 1 = remaining_alarm / timer (ej: alarm in 5 minutes)
long alarm = 0; //time in seconds

int selectedAlarm = 0;
int playingAlarm = 0;

//TIMESET
int selectedTimeSet = 0;

//SETTINGS 
bool sleep = false;
bool sound = true;
int selectedSettings = 0;

//PONG
int playerY = 30;
int comY = 30;
int ballY = 30;
int ballX = 42;
int ballDirX = 1;//1 if UP, -1 if DOWN
int ballDirY = 1;//1 if RIGHT , -1 if LEFT
int ballSpeed = 1;
int playerScore =0;
int comScore = 0;

//SNAKE
class Point{
    public:
        Point();
        Point(int x, int y);
        int x;
        int y;
};

Point::Point () {
  x = 0;
  y = 0;
}
Point::Point (int a, int b) {
  x = a;
  y = b;
}

Point head ;
Point body [100] ;
Point food ;
int snakeDir = 0; //0 if left, 1 if up, 2 if right, 3 if down;
int snakeScore = 0;
int BODY_SIZE = 100;

//FIX TIME
int fixTime = 0;

class Watch: public Screen
{
    private:

    int selectedWatch;
    void drawAnalog(){
        
        int centerX = 23;
        int centerY = 24;
        int size = 23; //out circle
        
        display.drawCircle(centerX, centerY, size-2, BLACK);
        display.drawCircle(centerX, centerY,size, BLACK);

        for (int i = 0; i < 12; i++ ){
            int pointX = degToClockX(clock12ToDeg(i),size-4);
            int pointY = degToClockY(clock12ToDeg(i),size-4);
            display.drawPixel(centerX+pointX,centerY-pointY, BLACK);
        }
        
        int hourX = degToClockX(clock12ToDeg(hour()),size-10);
        int hourY = degToClockY(clock12ToDeg(hour()),size-10);
        display.drawLine(centerX, centerY, centerX+hourX, centerY-hourY, BLACK);
        
        int minX = degToClockX(clock60ToDeg(minute()),size-6);
        int minY = degToClockY(clock60ToDeg(minute()),size-6);
        display.drawLine(centerX, centerY, centerX+minX, centerY-minY, BLACK);
        
        int secX = degToClockX(clock60ToDeg(second()),size-5);
        int secY = degToClockY(clock60ToDeg(second()),size-5);
        display.drawLine(centerX, centerY, centerX+secX, centerY-secY, BLACK);
        
        display.setCursor(52,30); 
        display.println(getDigit(day())+"/"+getDigit(month()));
        
        display.setCursor(40,40); 
        display.println(dayStr(4) );
    }
    void drawWatch(){
    
        display.setCursor(0,15); 
        display.println(getDigit(day())+"/"+getDigit(month()));

        display.setCursor(40,15); 
        display.println(dayStr(4) );

        display.setCursor(0,30); 
        display.setTextSize(2);
        display.println(getDigit(hour())+":"+getDigit(minute()));

        display.setCursor(60,38); 
        display.setTextSize(1);
        display.println(":"+getDigit(second() ));
    
    }
    public:
    Watch() {
        refresh = 1000;
       selectedWatch = 0;
    }
    void draw(){
        if (selectedWatch == 0) drawWatch();
        else drawAnalog();
    }
    void controller(){
        if (pushedButton(SELECT_PIN)) selectedWatch = (selectedWatch+1) %2;
    }
    void update(){}
    void enter() {}
    void exit() {}
};

class Pong: public Screen 
{
    public:
    Pong(){
        refresh = 50;
    }
    void draw(){
    
        //playfield
        display.drawFastHLine(0, 8, 84, BLACK);
        display.drawFastVLine(42, 8, 40, BLACK);
        
        //players
        display.fillRect(0,playerY,2,10,BLACK);
        display.fillRect(82,comY,2,10,BLACK);
    
        //ball
        display.fillRect(ballX,ballY,3,3,BLACK);
        
        //score
        display.setCursor(42-15,10); 
        display.println(playerScore);
        display.setCursor(42+5,10); 
        display.println(comScore);
        
        //ballspeed
        //display.setCursor(0,0); 
        //display.println("Speed "+String(ballSpeed));

    }

    void controller(){
        if (pushedButton(SELECT_PIN)) ballSpeed = (ballSpeed %3 )+1 ;
        if (pressedButton(UP_PIN))  playerY ++;
        if (pressedButton(DOWN_PIN))playerY --;
    }
    void update(){
        int diffX = ballDirX * ballSpeed;
        int diffY = ballDirY* ballSpeed;
        ballX += diffX;
        ballY += diffY;
        
        if (ballX +2 > WIDTH) {
            gameSound();
            if (ballY - diffY < comY || ballY - diffY > comY + 10)  {
                ballX = WIDTH/2;
                ballY = HEIGHT/2;
                playerScore++;
            } else {
                ballDirX = ballDirX * -1;   
            }
        }  
        if (ballX < 0) {
            
            gameSound();
            if (ballY - diffY < playerY || ballY - diffY > playerY + 10) {
                ballX = WIDTH/2;
                ballY = HEIGHT/2;
                comScore++;
            } else {
                ballDirX = ballDirX * -1;   
            }
        } 
        
        if (ballY +2 > HEIGHT || ballY < 10 ){
            gameSound();
            ballDirY = ballDirY * -1;   
        }
        
        //com AI
        if (ballX > WIDTH/2) comY += (ballY > comY+5)?1:-1;
    }
    void enter() {}
    void exit() {}
};

class Snake: public Screen 
{
    private:
    int random(int from, int to ){
        return (rand() % (to)) +from ;
    }
    bool collision (int x1, int y1, int x2, int y2, int m){
        return (((x1 >= x2 - m ) && (x1 <= x2 + m ) )&& 
        ((y1 >= y2 - m ) && (y1 <= y2 + m ) ));
    }
    void resetSnake(){
        for (int i = 0 ; i< BODY_SIZE; i++){
            body[i] =  Point();
        }
        head = Point(WIDTH/2,HEIGHT/2);
        snakeScore = 0;
        
        snakeDir = 0;
        
        //food =  Point(30,HEIGHT/2);
        food.x = random(2, 80);
        food.y = random(10, 34);
        //if (sound) buzzer(1);
        Sleepy::loseSomeTime(1000);
        
    }
    public:
    Snake(){
        refresh = 50;
    }
    void draw() {
    
        //playfield
        display.drawFastHLine(0, 8, 84, BLACK);
        
        //snake
        display.fillRect(head.x,head.y,3,3,BLACK);
        //body
        for (int i = 0 ; i<= snakeScore -1; i++){
            display.fillRect(body[i].x,body[i].y,3,3,BLACK);
        }
        //food
        display.fillRect(food.x,food.y,3,3,BLACK);

        //ballspeed
        //display.setCursor(0,0); 
        //display.println("Score "+String(snakeScore));
    }
    void controller(){
        //if (pushedButton(SELECT_PIN)) ;
        if (pushedButton(UP_PIN))   snakeDir = (snakeDir +1) %4;
        if (pushedButton(DOWN_PIN)) {
            snakeDir--;
            if (snakeDir <0) snakeDir = 3;
        }
    }

    void update(){
        
        //0 if left, 1 if up, 2 if right, 3 if down;
        int speed= 2;
        switch(snakeDir){
            case 0:
                head.x -= speed;
                break;
            case 1:
                head.y -= speed;
                break;
            case 2:
                head.x += speed;
                break;
            case 3:
                head.y += speed;
                break;
        }
        
        //update body
        Point lastPosition = head;
        Point tempPosition = head;
        for (int i = 0 ; i<= snakeScore -1; i++){
            //detect collision
            if (collision(head.x, head.y, body[i].x, body[i].y,0)){
                resetSnake();
                break;
            }
                
            tempPosition = body[i];
            body[i] = lastPosition;
            lastPosition = tempPosition;
        }
        if (collision(head.x,head.y,food.x,food.y,1)){
            body[snakeScore] = Point(head.x,head.y);
            snakeScore++;
            food.x = random(2, 80);
            food.y = random(10, 34);
        
            gameSound();
        }

        if ((head.x +2 > WIDTH) || (head.x < 0) ||  (head.y < 8) || (head.y +2 > HEIGHT) ) resetSnake();
    }
    void enter() {}
    void exit() {}
};

class TimeSet: public Screen 
{
    public:
    TimeSet(){}
    ~TimeSet(){}
    void draw() {
        display.drawRoundRect(10+20*(selectedTimeSet%3),selectedTimeSet<3?15:30,(selectedTimeSet==5)?30:18,18,2 , BLACK);
        
        //print alarm time
        display.setCursor(13,20);
        display.println(getDigit(hour())+":");
        display.setCursor(33,20); 
        display.println(getDigit(minute())+":" );
        display.setCursor(53,20); 
        display.println(getDigit(second() ) );
        
        display.setCursor(13,35);
        display.println(getDigit(day())+"/");
        display.setCursor(33,35); 
        display.println(getDigit(month())+"/" );
        display.setCursor(53,35); 
        display.println(year() );
    }
    
    void controller(){
        if (pushedButton(SELECT_PIN)) selectedTimeSet = (selectedTimeSet+1) %6;
        else if (pressedButton(UP_PIN) || pressedButton(DOWN_PIN) ){
            int inc = pressedButton(UP_PIN)?1:-1;
                
            long secondsInDay = 86400;
            switch(selectedTimeSet){
                case 0: adjustTime(inc*60*60);  break;
                case 1: adjustTime(inc*60); break;
                case 2: adjustTime(inc); break;
                case 3: adjustTime(inc*secondsInDay); break;
                case 4: adjustTime(inc*secondsInDay*30);break;
                case 5: adjustTime(inc*secondsInDay*365);  break;
            } 
        }
    }
    void update(){}
    void enter() {}
    void exit() {}
};

class AlarmSet: public Screen 
{
    public:
    AlarmSet(){}
    ~AlarmSet(){}
    void draw() {

        display.drawRoundRect(20*selectedAlarm,19,18,18,2 , BLACK);
    
        //Alarm modes
        switch( alarm_mode) {
            case 0 :display.drawBitmap(0, 20 , getImage(ALARM),16,16, BLACK); break;
            case 1 :display.drawBitmap(0, 20 , getImage(TIMER),16,16, BLACK); break;
        }
    
        //print alarm time
        display.setCursor(23,25);
        display.println(getDigit(hour(alarm))+":");
        display.setCursor(43,25); 
        display.println(getDigit(minute(alarm))+":" );
        display.setCursor(63,25); 
        display.println(getDigit(second(alarm) ) );
    }
    
    void controller(){
        if (pushedButton(SELECT_PIN)) selectedAlarm = (selectedAlarm+1) %4;
        if ((pushedButton(UP_PIN) || pushedButton(DOWN_PIN)) && (selectedAlarm==0) ){
           alarm_mode = (alarm_mode +1 )%2; updateAlarmMode();
        }
        if (pressedButton(UP_PIN)) switch(selectedAlarm){
            case 1: alarm = alarm + 3600;break;
            case 2: alarm = alarm +60; break;
            case 3: alarm++; break;
        } 
        if (pressedButton(DOWN_PIN)) switch(selectedAlarm){
            case 1: alarm = (hour(alarm) <= 0)?(59*60):(alarm-3600); break;
            case 2: alarm = (minute(alarm) <= 0)?(59):(alarm-60);  break;
            case 3: alarm = (second(alarm) <= 0)?(0):(alarm-1); break;
        }   
    }
    void update(){}
    void enter() {
        if (alarm < now() && !alarm_active) updateAlarmMode() ; 
    }
    void exit() {
        switch( alarm_mode) {
                case 0 : if (alarm > now() ) alarm_active = true; break;
                case 1 : if (alarm > 0 ) {
                    alarm_mode= 0 ;
                    alarm_active = true; 
                    alarm = now() + alarm; 
                }
                break;
            }
    }
};


Screen *sys_screens[] ={ 
                    new TimeSet(),
                    new Watch(),
//                    new Snake(),
//                    new Pong(),
                    new Settings(),
                    new AlarmSet()
};
          

int sysloop(Screen *user_screens[], const int alarm_melody[], const int alarm_durations[], const int ALARM_SIZE) {
    
    int sysscreens_size = (sizeof(sys_screens)/sizeof(*sys_screens));
    int usescreens_size = (sizeof(user_screens)/sizeof(*user_screens));
    int screens_size = sysscreens_size + usescreens_size;
    
    Screen* screens[screens_size] ;
    
    for(int i=0;i<sysscreens_size;i++){
        screens[i] = sys_screens[i];
    }
    for(int i=0;i<usescreens_size;i++){
        screens[sysscreens_size+i] = user_screens[i];
    }
    
    init(); // don't forget this!
    {
        if (DEBUG) Serial.begin(9600);
        pinMode(WAKE_PIN, INPUT); digitalWrite(WAKE_PIN,HIGH);

        pinMode(UP_PIN, INPUT); digitalWrite(UP_PIN,HIGH);
        pinMode(DOWN_PIN, INPUT);digitalWrite(DOWN_PIN,HIGH);
        pinMode(SELECT_PIN, INPUT); digitalWrite(SELECT_PIN,HIGH);

        setTime(00,0,0,30,8,13);
        buzzer(1);

        display.begin();
        display.setContrast(contrast);

        attachInterrupt(0, wakeUp, LOW);
        refresh = 100;

    } // END INIT

    
    
    while(1) {

        display.clearDisplay();
        
        controller(screens,screens_size);
        
        checkSleep();
        
        printStatusBar();
        
        screens[screen]->draw();
        screens[screen]->controller();
        screens[screen]->update();

        checkAlarm(alarm_melody, alarm_durations, ALARM_SIZE);
        fixDelay();
        count++;
        display.display();
        
        if (isAlarmSounding()) Sleepy::loseSomeTime(100); 
        else Sleepy::loseSomeTime(screens[screen]->getRefresh()); 

    } //END WHILE
}

void checkSleep(){
    if (sleep){
        if(count >= 1000/refresh*SLEEP_TIME ) {
        
            printPowerDown();
    
            if (alarm_active)  Sleepy::loseSomeTime((alarm - now())*1000);
            else Sleepy::loseSomeTime(60000);
            
            display.setContrast(contrast);
            
        } else{
            count ++;
        }
    }
}


void controller(Screen *screens[], int sizeof_screens){
    if (pressedButton(SELECT_PIN) ||
        pressedButton(UP_PIN) ||
        pressedButton(DOWN_PIN)) {
        count = 0;
        if (isAlarmSounding()) alarm_active = false;
    }
    if (pressedButton(WAKE_PIN)){

        count = 0;
        screens[screen]->exit();
        
        screen = (screen +1) % sizeof_screens;
        
        screens[screen]->enter();
     
    }
}

void wakeUp()
{
    // Just a handler for the pin interrupt.
    count = 0;
}

void fixDelay(){
    if (hour() == 0 && minute() == 0){
        adjustTime(fixTime);
    }
}

void updateAlarmMode(){
    switch(alarm_mode){
        case 0: alarm = now() ;break;
        case 1: alarm = 0;  break;
    }
}

void buzzer(int times){
    for (int i = 0 ; i < times ; i++){
        tone(BUZZERPIN, NOTE_C4, NOTE_DURATION);
        if (times > 1) delay(DELAY_BETWEEN_NOTES);
    } 
}

void gameSound(){
     if (sound) {
         tone(BUZZERPIN, NOTE_C4, NOTE_DURATION);
         delay(10);
         noTone(BUZZERPIN);
     }
}
void printPowerDown(){

    display.clearDisplay();
    display.setContrast(0);

}

void checkAlarm(const int alarm_melody[], const int alarm_melody_durations[], const int ALARM_SIZE){
    if (isAlarmSounding())playAlarm(alarm_melody,alarm_melody_durations, ALARM_SIZE);
}
bool isAlarmSounding(){
    return alarm_active && alarm <= now() ;
}


void printStatusBar(){
    printBarTime();
    printBarBattery();
    if (alarm_active) printBarAlarm();
   // display.drawFastHLine(0, 7, 84, BLACK);
}
//@Deprectated
void printBarDate(){
    display.setCursor(0,0); 
    display.println(getDigit(day())+"/"+getDigit(month()));
}

void printBarTime(){
   // display.setCursor(54,0); 

    String separator = " ";
    if (second() % 2 == 0 ) separator = ":";
    
    display.setCursor(58,0); display.println(getDigit(hour()));
    display.setCursor(68,0); display.println(separator);
    display.setCursor(72,0); display.println(getDigit(minute()));
    
    //display.println(getDigit(hour())+separator+getDigit(minute()));  
    //display.drawRoundRect(52,-1,50,10,2 , BLACK);
}

void printBarBattery(){
    //3300 is the minimum battery (bat = 0%)
    //4000 (3300 + 700) is the maximum (bat = 100%)
    //(vcc - 3300 ) * 100 / 700 = (vcc - 3300) / 7
    //We divide by 10 to get a number between 0 an 10
    int bat = (int) (readVcc() - 3300)/70;

    //battery case
    display.drawRect(41, 0, 10, 7, BLACK);
    //battery nipple
    display.drawRect(50, 1, 3, 5, BLACK);
    //fill
    display.fillRect(41, 0, min(bat,10), 7, BLACK); 
  
}
void printBarAlarm(){
    display.drawBitmap(0, 0 , getImage(SOUND),10,10, BLACK);
    //display.drawRoundRect(42,-1,11,10,2 , BLACK);
}

void playAlarm(const int alarm_melody[], const int alarm_melody_durations[], const int ALARM_SIZE){

    int noteDuration = alarm_melody_durations[playingAlarm]* 1000/8 ;
    tone(BUZZERPIN, alarm_melody[playingAlarm],noteDuration);
    delay(noteDuration * 0.6);
    noTone(BUZZERPIN);
  
    count = 0;
    if (playingAlarm == ALARM_SIZE -1 ){
        delay(1000);
        noTone(BUZZERPIN);   
        playingAlarm  = 0;
    } else {
        playingAlarm++ ;
    }
}

void debug(String s ){
   if (DEBUG) Serial.println(s); 
}
void debug(int s ){
   if (DEBUG) Serial.println(s); 
}

long readVcc() {
  // Read 1.1V reference against AVcc
  // set the reference to Vcc and the measurement to the internal 1.1V reference
  #if defined(__AVR_ATmega32U4__) || defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
    ADMUX = _BV(REFS0) | _BV(MUX4) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  #elif defined (__AVR_ATtiny24__) || defined(__AVR_ATtiny44__) || defined(__AVR_ATtiny84__)
    ADMUX = _BV(MUX5) | _BV(MUX0);
  #elif defined (__AVR_ATtiny25__) || defined(__AVR_ATtiny45__) || defined(__AVR_ATtiny85__)
    ADMUX = _BV(MUX3) | _BV(MUX2);
  #else
    ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  #endif  
 
  delay(2); // Wait for Vref to settle
  ADCSRA |= _BV(ADSC); // Start conversion
  while (bit_is_set(ADCSRA,ADSC)); // measuring
 
  uint8_t low  = ADCL; // must read ADCL first - it then locks ADCH  
  uint8_t high = ADCH; // unlocks both
 
  long result = (high<<8) | low;
 
  result = 1125300L / result; // Calculate Vcc (in mV); 1125300 = 1.1*1023*1000
  return result; // Vcc in millivolts
}