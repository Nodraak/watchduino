#ifndef utils_h 
#define utils_h 

#include "screen.h"

class Settings: public Screen
{
    void draw();
    void controller();
    void update();
    void enter();
    void exit();
};

#endif