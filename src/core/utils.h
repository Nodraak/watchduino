#ifndef _utils_h 
#define _utils_h 

#include <WString.h>
#include "Arduino.h" 

String getDigit(int digits);
bool pressedButton(int pin);
bool pushedButton(int pin);

int clock12ToDeg(int clockVal);
int clock60ToDeg(int clockVal);
int degToClockX(int degree, int r);
int degToClockY(int degree, int r);
float rad (int degree);

#endif