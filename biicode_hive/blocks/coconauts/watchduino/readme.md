# WatchDuino

WatchDuino is an open hardware project that combines inexpensive electronic 
components and a complex Arduino (C++) code to build a useful and reprogrammable
smart watch.

The code and the components have been optimized after a lot of prototypes to 
provide a rich set of features with a small and cheap battery that can last more
than a week without recharging. A lot of electronic and software engineering was
required to make this project possible.

# Demonstrative video

[Watch on Youtube](https://www.youtube.com/watch?v=CtgR1YiwnEY), it's pretty awesome!

# Features

- Time and date (analog and digital output)
- Alarm / Countdown (with custom music)
- Games
    - Pong (1 vs com) 
    - Snake
- Rechargeable battery (by USB)
- Battery meter
- Low-battery mode (it can last 2 years with 240mAh battery)
- Built-in screen light
- Compact design

# Main components

- ATMega 328
- Crystal oscillator (16Mhz)
- LiPo battery (240mAh) 
- Nokia 5110 LCD screen 

Full list on the ["how to replicate" document](http://bitbucket.org/rephus/watchduino/src/master/docs/how_to_replicate.md).

# Development repository

[On Bitbucket](http://bitbucket.org/rephus/watchduino)

# Documentation

[Project motivation](http://bitbucket.org/rephus/watchduino/src/master/docs/motivation.md) - Why we built WatchDuino, what does it offer to the world, and some technical musings.

[How to install](http://bitbucket.org/rephus/watchduino/src/master/docs/install.md) - How to set up your system to make it ready to work with WatchDuino.

[How to replicate](http://bitbucket.org/rephus/watchduino/src/master/docs/how_to_replicate.md) - Components, libraries, and everything you need to build your own WatchDuino.

[How to program](http://bitbucket.org/rephus/watchduino/src/master/docs/how_to_program.md) - Hack on WatchDuino's software to make your own apps and customizations.

# Biicode details

This block has the following block dependencies:

- [adafruit/gfx_library](http://www.biicode.com/adafruit/blocks/adafruit/gfx_library/branches/master)
- [coconauts/adafruit_pcd8544](http://www.biicode.com/coconauts/blocks/coconauts/adafruit_pcd8544/branches/master) - Originaly [adafruit/pcd8544_nokia_5110_lcd_library](http://www.biicode.com/adafruit/blocks/adafruit/pcd8544_nokia_5110_lcd_library/branches/master), but needed to be customized.
- [coconauts/jeelib](http://www.biicode.com/coconauts/blocks/coconauts/jeelib/branches/master) - A working copy of the [Jeelib utility library](http://github.com/jcw/jeelib).
- [coconauts/time](https://www.biicode.com/coconauts/blocks/coconauts/time/branches/master) - A working copy of [Teensy Time library](http://www.pjrc.com/teensy/td_libs_Time.html).
- [coconauts/arduino](http://www.biicode.com/coconauts/blocks/coconauts/arduino/branches/master) - Contains some Arduino core libraries that are not yet tracked by Biicode, so they need to be provided externally.

It's also prepared to be used as framework: we tried to make it as easy as possible to make your own derivate using Biicode. You can see an example block which includes this one as a dependency in [coconauts/watchduino_hello_world](http://www.biicode.com/coconauts/blocks/coconauts/watchduino_hello_world/branches/master). This block extends WatchDuino by providing a new custom screen.

The project documentation (in particular, the ["how to program" document](http://bitbucket.org/rephus/watchduino/src/master/docs/how_to_program.md)) details how program this customizations, using either Biicode or the regular Arduino tooling. 
