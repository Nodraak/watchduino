#ifndef screen_h 
#define screen_h

class Screen
{
    protected:
        int refresh;
    public:
        Screen(){
            refresh = 100;
        }
        int getRefresh(){
            return refresh;
        }
        virtual ~Screen(){}
        virtual void draw() = 0;
        virtual void controller() = 0;
        virtual void update() = 0;
        virtual void enter() = 0;
        virtual void exit() = 0;
};

#endif